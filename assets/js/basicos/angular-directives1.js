var $baseUrl = $('div#base-url').html();

var app = angular.module('myApp', []);

app.directive("testDirectiva", function() {
    return {
        template : "<h2>Hecho con una directiva!</h2>"
    };
});

app.controller('myController', ['$scope', function($scope){
  console.log({"Controlador": "Ejemplo directivas"});

}]);