var $baseUrl = $('div#base-url').html();

var app = angular.module('myApp', []);

app.filter('split', function() {
    return function(input, splitChar, splitIndex) {
        if(typeof input != 'undefined' && input !== '')
            return input.split(splitChar)[splitIndex];
    }
});

app.controller('myController', ['$scope', '$filter', function($scope, $filter){
  console.log({"Controlador": "Ejemplo filtros"});
  
  $scope.text = 'texto - a - filtrar';
  var resultSplit = $filter('split')(text, ' - ', 0);

}]);