defined('BASEPATH') OR exit('No direct script access allowed');

class Get extends CI_Controller {

  function __construct()
	{
		parent::__construct();
	}
  
	public function index()
	{
    $data["data"]["view"] = "index";
    // $data["iduser"] = $this->session->id_user;
		$this->load->view('/template/response/jsonfile', $data);
	}
  
  public function dataview()
	{
    $data["data"] = array(
      "view" => "dataview",
      "title" => "Javascript",
      "type" => "language-css",
      "content" => "p { color: blue }",
      "response" => TRUE
    );
    // $data["iduser"] = $this->session->id_user;
		$this->load->view('/template/response/jsonfile', $data);
	}
	
	public function departamentos($params = 'index')
	{
    $data['data']["view"] = $params;
		$data['data']["response"] = true;
		$data["data"]["departamentos"] = array(
			array("id" => "1", "nombre" => "ANTIOQUIA"),
			array("id" => "2", "nombre" => "ATLANTICO"),
			array("id" => "3", "nombre" => "BOLIVAR")
		);
    // $data["iduser"] = $this->session->id_user;
		$this->load->view('/template/response/jsonfile', $data);
	}
	
	public function municipios($params = 'index')
	{
   $data['data']["view"] = $params;
		$data['data']["response"] = true;
		$data["data"]["municipios"] = array();

		$municipios = array(
			"1" => array(
			array("id" => "1", "nombre" => "MEDELLIN"),
			array("id" => "2", "nombre" => "ABEJORRAL"),
			array("id" => "3", "nombre" => "ABRIAQUI"),
				),
			"2" => array(
			array("id" => "4", "nombre" => "GALAPA"),
			array("id" => "5", "nombre" => "JUAN DE ACOSTA"),
			array("id" => "6", "nombre" => "LURUACO"),
				),
			"3" => array(
			array("id" => "7", "nombre" => "ACHI"),
			array("id" => "8", "nombre" => "ALTOS DEL ROSARIO"),
			array("id" => "9", "nombre" => "ARENAL"),
				),
			);

		if(!empty($params)){
			$data["data"]["municipios"] = $municipios[$params];
		}
		
		$this->load->view('/template/response/jsonfile', $data);
	}
	
	
}